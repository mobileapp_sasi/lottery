import 'package:dart_application_lottery/dart_application_lottery.dart'
    as dart_application_lottery;
import 'dart:io';
import 'dart:math';

void main() {
  var winList = {
    '1stPrice': '436594',
    '2ndPrice': '502412',
    '3rdPrice': '396501',
    '4thPrice': '285563',
    '5thPrice': '084971',
    '6thPrice': '575619',
    '7thPrice': '610343',
    '8thPrice': '491669',
    '9thPrice': '917404',
    '10thPrice': '327644'
  };
  print('Please input num :');
  var num = stdin.readLineSync()!;
  if (isWin(num, winList)) {
    print('$num is win in ${getPrice(num, winList)}');
  } else {
    print('$num is not win');
  }
}

bool isWin(num, winList) {
  for (var value in winList.values) {
    if (num == value) {
      return true;
    }
  }
  return false;
}

String getPrice(num, winList) {
  var price = winList.keys.firstWhere((k) => winList[k] == '$num');
  return price;
}
